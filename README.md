# Bitbucket Pipelines Pipe Example: Azure Web Apps Containers Deploy

This sample uses the Bitbucket Azure Web Apps Containers Deploy pipeline to deploy a sample container with a Python Django app to [Azure Web Apps for Containers](https://azure.microsoft.com/en-us/services/app-service/containers/). The custom image uses port 8000.

## Prerequisites

You will need to configure required Azure resources before running the pipe. The easiest way to do it is by using the Azure cli. You can either [install the Azure cli](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest) on your local machine, or you can use the [Azure Cloud Shell](https://docs.microsoft.com/en-us/azure/cloud-shell/overview) provided by the Azure Portal in a browser.

### Service principal

You will need a service principal with sufficient access to create or an Azure App Service, or update an existing App Service. To create a service principal using the Azure CLI, execute the following command in a bash shell:

```sh
az ad sp create-for-rbac --name MyServicePrincipal
```

Refer to the following documentation for more detail:

* [Create an Azure service principal with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)

### Azure App Service app

Using the service principal credentials obtained in the previous step, you can use the following commands to create an Azure App Service instance in a bash shell:

```bash
az login --service-principal --username ${AZURE_APP_ID}  --password ${AZURE_PASSWORD} --tenant ${AZURE_TENANT_ID}

az group create --name ${AZURE_RESOURCE_GROUP} --location australiaeast

az appservice plan create --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --sku B1 --is-linux

az webapp create --name ${AZURE_APP_NAME} --resource-group ${AZURE_RESOURCE_GROUP} --plan $AZURE_APP_NAME

az webapp config appsettings set --resource-group ${AZURE_RESOURCE_GROUP} --name ${AZURE_APP_NAME} --settings WEBSITES_PORT=${WEBSITES_PORT}
```

Refer to the following documentation for more detail:

* [Use a custom Docker image for Web App for Containers](https://docs.microsoft.com/en-us/azure/app-service/containers/tutorial-custom-docker-image)

## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
